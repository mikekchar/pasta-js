// # pasta.js
// Copyright © 2022 Mike Charlton
// The MIT License (MIT)
// See LICENSE in main project for more information.

// A "cut-and-paste" functional programming front-end framework for modern
// browsers.  Documentation is in the main project.

const zip = Array_A => Array_B => // Array_Array_AB
  Array .from (Array_A) .map ((A, i) => [A, Array_B[i]])
;

const curry = f_A$B => Array_A => // B
  Array_A .reduce ((g_A$B, A) => g_A$B(A), f_A$B)
;

class Maybe extends Array { }
Maybe .prototype .bind = Maybe .prototype .flatMap

const some = A => // Maybe_A
  Maybe .from ([A])
;

const none = () => // Maybe_A
  Maybe .from ([])
;

const namespace = $B => // B
  $B ()
;

// Note: tap is usually used to mutate A
const tap = A => A$ => // A
  (_ => A) (A$ (A))
;

const test = namespace(
  () => {
    const assert = A$Bool => Array_A => // Maybe_Array_A
      curry (A$Bool)(Array_A)
        ? none () 
        : some (Array_A)
    ;

    // The returned function will mutate itself when called
    const mock = returns_B => // A$B
    {
      const A$B = passed_A => tap (returns_B)(_ => A$B .last = passed_A)
      return A$B;
    };

    const eql = actual_A => expected_A => // Bool
      type (expected_A) === type (actual_A) &&
        type (expected_A) .eql (actual_A)(expected_A)
    ;

    const toString = A => // String
      type (A) .toString (A)
    ;

    const type_name = A => // String
      typeof (A) === 'object'
        ? A .constructor .name
        : typeof(A)
    ;

    const type = A => // Variant
      // Order is important here
      [stringType, arrayType, dictType, defaultType] .find (x => x .is (A))
    ;

    const stringType = {
      is: A => // Bool
      type_name (A) === 'String',

      eql: a_A => b_A => // Bool
      a_A === b_A,

      toString: A => //String
      `'${A}'`
    };

    const arrayType = {
      is: A => // Bool
      Array .isArray (A),

      eql: a_A => b_A => // Bool
      a_A .length === b_A .length &&
      zip (a_A)(b_A) .every (curry (eql)),

      toString: A => // String
      `[${A .map (toString) .join (', ')}]`,
    };

    const dictType = {
      is: A => // Bool
      type_name (A) === 'Object',

      eql: a_A => b_A => // Bool
      arrayType .eql (Object .entries (a_A))(Object .entries (b_A)),

      toString: A => // String
      `{${Object .entries(A) .map (x => x[0] + ': ' + toString (x[1])) .join (', ')}}`,
    };

    const defaultType = {
      is: _ => // Bool
      true,

      eql: a_A => b_A => // Bool
      a_A === b_A,

      toString: A => //String
      `${A}`
    };

    return {
      assert: assert,
      eql: eql,
      mock: mock
    };
  }
);

const example = label_String => returns_B => // Example
  namespace(
    () => {
      const { assert, eql } = test;

      const outcome = label_String => err_Maybe_String => // Outcome
        ({ label: label_String, result: err_Maybe_String })
      ;

      const returns = expected_B => // Outcome
        outcome (label_String) (assert (eql) ([returns_B, expected_B]))
      ;

      const satisfies = B$Bool =>  // Outcome
        outcome (label_String) (assert (B$Bool) ([returns_B]))
      ;

      return {returns: returns, satisfies: satisfies};
    }
  )
;

// Note: Mutates A$B
const def = A$B => $Array_Outcome => // A$B
  tap(A$B)(_ => A$B .outcomes = $Array_Outcome)
;

const format = Outcome => // String
  isNone(Outcome .result)
    ? `PASS - ${Outcome .label}`
    : `FAIL (${Outcome .result .toString ()}) - ${label_String}`
;

const testRunner = defs_Array_A$B => format_Outcome$String => // Array_String
  defs_Array_A$B .flatMap (
    A$B => A$B .outcomes() .map(
      outcome => format_Outcome$String (outcome)
    )
  )
;

const dbg = (A, String$Console = console.log) => // A + Console effect
  tap (A) (String$Console)
;

const compact = Array_A => // Array_A
  Array_A .filter (A => A !== undefined && A !== null)
;

const isNone = Maybe_A => // Bool
  Maybe_A.length === 0
;

const maybe = A$Bool => A => // Maybe_A
  some(A) .filter (A$Bool)
;

const sequence = Array_Maybe_A => // Maybe_Array_A
  Array_Maybe_A.reduce(
    (Maybe_Array_A, Maybe_A) => (
      isNone (Maybe_Array_A) || isNone (Maybe_A)
      ? none ()
      : tap (Maybe_Array_A) (Maybe_Array_A => Maybe_Array_A[0] .push (Maybe_A[0]))
    ), some ([])
  )
;

const notNaN = // a => Mnum
  maybe(a => !isNaN(a))
;

const tryNum = a => // Mnum
  notNaN(parseFloat(a))
;

const isSet = // a => Ma
  maybe(a => a !== null && a !== undefined)
;

const getElement = doc => id => //Melement
  isSet(doc.getElementById(id))
;

const captureInput = doc => id => callback => // Melement + dom_effect
  getElement(doc)(id).map(
    element => tap(element) (
      input => input.addEventListener('input', callback)
    )
  )
;

const value = doc => id => // Mstring
  getElement(doc)(id).bind(
    el => isSet(el.value)
  )
;

const setValue = doc => id => value => // value
  tap(value) (
    value => getElement(doc)(id).bind(
      el => el.value = value.toString()
    )
  )
;

const numValues= doc => ids => f =>
  sequence(ids.map(x => tryNum(value(doc)(x)))).map(
    curry(f)
  )
;

const publish = doc => id => html => // Melement + page_effect
  tap(getElement(doc)(id)) (
    Melement => Melement.bind(
      el => el.innerHTML = html
    )
  )
;

const element = doc => (type, attrs = {}) => // element
  tap(doc.createElement(type)) (
     el => Object.entries(attrs).forEach(attr => el.setAttribute(...attr))
  )
;

const entry = doc => (type, attrs = {}) => // html
  element(doc)(type, attrs).outerHTML
;

const container = doc => (type, attrs = {}) => (...html_list) => // html
  tap(element(doc)(type, attrs)) (
    el => el.innerHTML = html_list.join("\n")
  ).outerHTML
;

const div = doc => (...html_list) => // html
  container(doc)('div')(...html_list)
;

const li = doc => (...html_list) => // html
  container(doc)('li')(...html_list)
;

const ul = doc => strings => // html
  container(doc)('ul') (
    strings.map(
      string => li(doc)(string)
    ).join("\n")
  )
;

const addClass = klass => attrs => // attrs
  ({attrs, ...{'class': compact([klass, attrs['class']]).join(' ')}})
;

const contents = doc => attrs => (...html_list) => // html
  container(doc)('div', addClass('contents')(attrs))(...html_list)
;

const drop = doc => (type, attrs = {}) => heading => (...html_list) => // html
  container(doc)('label', {'class': 'drop'})(
    entry(doc)('input', {'type': 'checkbox', 'class': 'collapse'}),
    container(doc)(type, {'class': 'heading'})(heading),
    contents(doc)(attrs)(...html_list)
  )
;

export {
  zip,
  curry,
  Maybe,
  some,
  none,
  namespace,
  test,
  example,
  def,
  format,
  testRunner,
  tap,
  dbg,
  compact,
  isNone,
  maybe,
  sequence,
  notNaN,
  tryNum,
  isSet,
  getElement,
  captureInput,
  value,
  setValue,
  numValues,
  publish,
  element,
  entry,
  container,
  div,
  li,
  ul,
  addClass,
  contents,
  drop
};
