// # pasta_test.mjs
// Copyright © 2022 Mike Charlton
// The MIT License (MIT)
// See LICENSE in main project for more information.

import {
  zip,
  curry,
  some,
  none,
  namespace,
  test,
  example,
  testRunner,
  tap,
  dbg,
  compact,
  isNone,
  maybe,
  sequence,
  notNaN,
  tryNum,
  isSet,
  getElement,
  captureInput,
  value,
  setValue,
} from './pasta.mjs'

zip.outcomes = () => [
  example ('zip mixes 2 arrays') (
    zip([1, 2, 3])([4, 5, 6])
  ).returns( [[1, 4], [2, 5], [3, 6]] ),

  example ('zip discards extra elements in the second array') (
    zip([1, 2])([4, 5, 6])
  ).returns( [[1, 4], [2, 5]] ),

  example ('zip uses undefined to pad out the second array') (
    zip([1, 2, 3])([4, 5])
  ).returns( [[1, 4], [2, 5], [3, undefined]] ),
];

curry.outcomes = () => [
  example ('curry applies paramters from an array one at a time') (
    curry(x => y => z => (x - y) * z) ([5, 3, 2])
  ).returns( 4 ),
];

const bootstrapOutcomes = // format => strings
  testRunner([zip, curry])
;

tap.outcomes = () => [
  example ('tap applies the value to the function always returning the original') (
    tap(4)(x => x + 5)
  ).returns( 4 ),

  example ('tap can mutate the original value') (
    tap([4])(x => x[0] = 12)
  ).returns( [12] ),
];

const mockLog = test.mock(undefined);

dbg.outcomes = () => [
  example ('dbg outputs the value to the log') (
    dbg(42, mockLog)
  ).satisfies( _ => test.eql(mockLog.last)(42) ),

  example ('dbg returns the value') (
    dbg(42, mockLog)
  ).returns( 42 ),
];

compact.outcomes = () => [
  example ('compact removes undefined entries in an array') (
    compact([1, undefined, 2])
  ).returns( [1, 2] ),
  example ('compact removes null entries in an array') (
    compact([1, null, 2])
  ).returns( [1, 2] ),
  example ('compact does not remove 0 entries in an array') (
    compact([1, 0, 2])
  ).returns( [1, 0, 2] )
];

const fpOutcomes = // format => strings
  testRunner([tap, dbg, compact])
;

some.outcomes = () => [
  example ('some wraps a value') (
    some(42)
  ).returns( [42] ),
  example ('some acts like a functor') (
    some(42).map(num => num + 5)
  ).returns( [47] ),
  example ('some acts like a monad') (
    some(42).bind(num => some(num + 5))
  ).returns( [47] ),
];

none.outcomes = () => [
  example ('none wraps nothing') (
    none()
  ).returns( [] ),
  example ('some acts like a functor') (
    none().map(num => num + 5)
  ).returns( [] ),
  example ('some acts like a monad') (
    none().bind(num => some(num + 5))
  ).returns( [] ),
];

isNone.outcomes = () => [
  example ('isNone returns true if it is none') (
    isNone(none())
  ).returns( true ),
  example ('isNone returns false if it is some') (
    isNone(some(42))
  ).returns( false ),
];

maybe.outcomes = () => [
  example ('maybe returns some if filter true') (
    maybe(x => x % 2 === 0)(6)
  ).returns( some(6) ),
  example ('maybe returns none if filter false') (
    maybe(x => x % 2 === 0)(5)
  ).returns( none() ),
];

sequence.outcomes = () => [
  example ('sequence takes an array of Maybes and returns a Maybe with an Array') (
    sequence([some(42), some(27), some(3)])
  ).returns( some([42, 27, 3]) ),
  example ('sequence returns some([]) if the array is empty') (
    sequence([])
  ).returns( some([]) ),
  example ('sequence returns none() if any element is none()') (
    sequence([some(42), none(), some(3)])
  ).returns( none() ),
];

const monadOutcomes = // format => strings
  testRunner([some, none, isNone, maybe, sequence])
;

notNaN.outcomes = () => [
  example ('notNaN returns some(a) if a is a number') (
    notNaN(42)
  ).returns( some(42) ),
  example ('notNaN returns none() if a is NaN') (
    notNaN(NaN)
  ).returns( none() ),
];

tryNum.outcomes = () => [
  example ('tryNum returns some(a) if a is a number') (
    tryNum(42)
  ).returns( some(42) ),
  example ('tryNum returns some(num) if a can be converted to a number') (
    tryNum('42')
  ).returns( some(42) ),
  example ('tryNum returns none() if a can not be converted to number') (
    tryNum('fortytwo')
  ).returns( none() ),
  example ('tryNum can convert some(num) to a number') (
    tryNum(some(42))
  ).returns( some(42) ),
  example ('tryNum can convert some(string) to a number') (
    tryNum(some('42'))
  ).returns( some(42) ),
];

isSet.outcomes = () => [
  example ('isSet returns some(a) if a is not null') (
    isSet(42)
  ).returns( some(42) ),
  example ('isSet returns none() if a is null') (
    isSet(null)
  ).returns( none() ),
  example ('isSet returns none() if a is undefined') (
    isSet(undefined)
  ).returns( none() ),
];

const jsOutcomes = // format => strings
  testRunner([notNaN, tryNum, isSet])
;

// ## DOM functions for creating fakes for outcomes
const fakeElement = value => mockListener => ({
  value: value,
  addEventListener: (type, callback) => mockListener([type, callback]),
  setAttribute: (_name, _value) => undefined,
  innerHTML: '',
  outerHTML: '',
});

const fakeDocument = map =>
  ({
    getElementById: id => map[id] || null,
    createElement: _type => fakeElement(null)(_ => undefined),
  })
;

getElement.outcomes = () => {
  const listener = test.mock(undefined);
  const element = fakeElement(42)(listener);
  const doc = fakeDocument({ fake_id: element })

  return [
    example ('getElement returns some(element) from the document if it exists') (
      getElement(doc)('fake_id')
    ).returns(some(element)),

    example ('getElement returns none if the element does not exist') (
      getElement(doc)('not_there')
    ).returns(none()),
  ];
};

captureInput.outcomes = () => {
  const callback = _ => null;
  const listener = test.mock(undefined);
  const element = fakeElement(42)(listener);
  const doc = fakeDocument({ fake_id: element })

  return [
    example ('captureInput returns the element') (
      captureInput(doc)('fake_id')(callback)
    ).returns(some(element)),

    example ('captureInput listens to input events on the element specified by the id') (
      captureInput(doc)('fake_id')(callback)
    ).satisfies(_ => test.eql(listener.last)(['input', callback])),
  ];
};

value.outcomes = () => {
  const listener = test.mock(undefined);
  const full_element = fakeElement(42)(listener);
  const null_element = fakeElement(null)(listener);
  const undefined_element = fakeElement(undefined)(listener);
  const doc = fakeDocument({
    fortytwo: full_element,
    null_id: null_element,
    undefined_id: undefined_element,
  });

  return [
    example ('value returns the value in the specified element') (
      value(doc)('fortytwo')
    ).returns(some(42)),
    example ('value returns none if the element is null') (
      value(doc)('null_id')
    ).returns(none()),
    example ('value returns none if the element is undefined') (
      value(doc)('undefined_id')
    ).returns(none()),
  ];
};

setValue.outcomes = () => {
  const listener = test.mock(undefined);
  const zero_element = fakeElement(0)(listener);
  const doc = fakeDocument({
    zero: zero_element,
  });

  return [
    example ('setValue sets the value of the element') (
      setValue(doc)('zero')(42)
    ).satisfies(_ => test.eql(value(doc)('zero'))(some('42'))),
    example ('setValue does nothing if the element does not exist') (
      setValue(doc)('not_there')(42)
    ).returns(42),
  ];
};

const domOutcomes = // format => strings
  testRunner([getElement, captureInput, value, setValue])
;

const logTestOutcomes = log => namespace(
  () => {
    const format = Outcome => // String
      isNone(Outcome .result)
        ? `PASS - ${Outcome .label}`
        : `FAIL (${Outcome .result .toString ()}) - ${label_String}`
    ;

    [
      bootstrapOutcomes,
      fpOutcomes,
      monadOutcomes,
      jsOutcomes,
      domOutcomes
    ].reduce(
      (results, outcomes) => results.concat(outcomes(format)), []
    ).forEach(x => log(x))
  }
);

logTestOutcomes(console.log)
