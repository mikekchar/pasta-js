# Pasta-JS TODO

## TODO

  - [X] Create initial repository with only Pasta code
  - [X] Push it to various public repositories
        Gitlab for now
  - [X] Separate tests in order to keep the size small
  - [X] Refactor test runner
  - [X] Put pasta_min in an ES6 module
  - [X] Make the tests pass
  - [X] move pasta_min.mjs it pasta.mjs
        The original pasta code is now gone
  - [X] Don't coerce errors to string in test.assert
