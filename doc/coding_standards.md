# Pasta JS Coding Standards

Pasta JS uses somewhat of a strange coding style.
Because for most people this will be hard to read at
first, I'm writing a guide to help understand.  First
to adress the elephant in the room:

## Why does it use this strange coding style?

Because that was the entire intent of the project.
I wanted to experiment with a functional coding style
that would allow someone to easily write a small
single page application with no dependencies.  The
idea was to be able to load the HTML directly off
of disk without a server and run the application.
The functional style is just a continuation of
several experiments I've done in that direction.

Consider all of this to be experimental.  I'm not
seriously expecting anyone to write code like this.
It's just an exploration.

## Function Declarations

Function definitions should look like this:

```
const foo = param_Type => // Return_Type
  some_code (param_Type)
;
```

Details are below.

### Prefer constant function variables

Functions should be assigned to `const` variables.
Avoid objects unless namespacing the functions (see below).

Example:

`const my_function = // Some function definition`

### Prefer functions with a single or no parameters

We will use currying extensively so single parameters make life
easier.

Example:

`const my_function = param_Type => // Rest of function definition`

Or:

`const my_function = () => // Rest of function definition`

If you must use multiple parameters, add defaults for all but the
first.  Example:

`const my_function = (param_Type, foo = 10) => // Rest of function`

This can be useful for testing in some cases.  The production code
should only use the first parameter.

### Avoid defaults for normal parameters

Defaults make it difficult to reason about currying.  Avoid:

`const my_function = (param_Type = 10) => // Rest of function`

### Prefer double arrow functions and avoid `this`

As much as possible we are going to use idempotent functions
with immutable data structures.  `this` gets in the way of
that.  Use ` => ` for creating functions.  When namespacing
functions, use the following notation:

```
const my_namespace = {
  my_function: param_Type => // Return_Type
    do_something (param_Type)
  ,
  next_function: // ...
};

my_namespace .my_function (param_Type);
```

### Prefer single expression functions

JS functions can be defined without using `{}` and `return` if they
use a single expression.  This creates much more compact code.  You
can display more code in a give screen area.

Prefer:

```
const my_function = param_Type => // Return_Type
  do_something (param_Type)
;
```

Avoid:

```
const my_function = param_Type => // Return_Type
{
  return(do_something (param_Type));
};
```

### Terminate statements with `;`

JS Allows you to elide `;`, but if you do so, you can no longer
start expressions with `[]`.  If you want to do
`[1, 2, 3].map(|i| some_function(i))` you have to do
`;[1, 2, 3].map(|i| some_function(i))`, which is weird and easy
to forget.

Because we are using single expression functions, the `;` also makes
a nice visual terminator for functions.  Place it alone on the last
line like:

```
const my_function = param_Type => // Return_Type
  do_something (param_Type)
;
```

### Avoid `if` and `switch`

Very unfortunately, `if` and `switch` are statements in JS and so
you can't use them if you are avoiding statements.  Instead use
the ternery operator.

Prefer:

```
my_condition
  ? do_then(something)
  : do_else(something)
```

### Avoid intermediate variables

Very unfortunately, assignment is a statement in JS.  This means
we must avoid:

```
const my_var = 123;
do_something (my_var);
```

Sometimes you can't avoid it, but often it is possible to avoid it
with `tap` (see the source code):

```
123.tap ( |my_var|
  do_something(my_var)
)
```

Note that `tap` will return `123` in this case, though.
