# Pasta-JS Documentation

## Functions

### Bootstrap

#### zip => as => bs => abs

`zip(['a', 'b', 'c'][1, 2, 3]) === [['a', 1], ['b', 2], ['c', 3]]`

#### curry => (a => b) => as => b

`curry(a => b => c => a + b + c, [1, 2, 3]) === (a => b => c => a + b + c)(1)(2)(3)`

#### Maybe

Implemented as an array that contains a single element.

`[a]` is some(a) and `[]` is none().

`some(31).map(x => x + 1)` returns `some(32)`

`none().map(x => x + 1)` returns `none()`

`some(31).bind(x => some(x + 1))` returns `some(32)`

`none().bind(x => some(x + 1))` returns `none()`

`some(31).bind(x => none())` returns `none()`

#### some => a => Ma

`some(a) === [a]`

It indicates that a value exists

#### none => () => Ma

`none() === []`

It indicates that a value does not exist

#### namespace => (() => a) => a

Namespace some code.
Example:
```
   const num = namespace(
     () => {
       const one = 1;
       return { inc: x => x + one };
     }
   );
   num.inc(5);
```

### Test

#### test.assert => ( a => a => ... => b ) => as => M(String of bs)

Runs the test with the values. Returns some(error) if the test failed.
Otherwise returns none().

#### test.mock a => a + memo

Returns a function that always returns stub_val. However it remembers the
parameter for the last invocation

#### test.eql a => a => bool

Returns true if actual and expected are equal.  Can theoretically exhaust
stack space, but is unlikely to do so in practice.

#### test.to_s a => string

Converts `a` to a string representation.

#### test.type_name => a => string

Converts `a` to a unique type name.

#### test.type => a => object

Converts `a` to an internal object representing `a`'s type

#### example => string => a => Example

Typical usage:
```
   example ('Example name')
     ( code_goes_here() ).returns(value)
```
Or:
```
   example ('Example name')
     ( code_goes_here() ).satisfies(function)
```

#### def => f => (() => examples) => f

Define a function along with tests.  Example:

```
   const inc = def (
     a => // a

     x + 1
   ) ( () => [
     example ('adds 1 to a number') (
       inc(5)
     ).returns( 6 )
   ]);
```

#### test_runner => fs => format => strings

Take an array of functions defined with def()
Return a function that takes a formatter and returns the test results
Here is an example formatter:

```
 label => Merror => // string
   is_none(Merror)
     ? `PASS - ${label}`
     : `FAIL (${Merror.toString()}) - ${label}
 ;
``
