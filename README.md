# Pasta-JS

A "cut-and-paste" functional programming front-end framework for modern
browsers.

## Description

The intent of Pasta-JS is to provide a minimal functional programming
front end for static single page web applications.  Because browsers
can't typically load more than a single file into the same memory
space when loading a static file, Pasta-JS is intended to be used
by copying and pasting the framework at the beginning of your code.
For that reason, it is as small as reasonable and contains no dependencies.

Pasta-js evolved from a variety of different experiments with writing
modern ES6 code in a functional programming way.  It has no dependencies
and probably is completely incompatible with every other Javascript
module.  As much as possible all code is implemented as curried lambdas
written as single expressions.  This gives Pasta code a very different
style from your typical Javascript code.

This description is simply a placeholder until I can get a better description
in place.  If you would like to see Pasta-JS in action, please see
https://gitlab.com/mikekchar/calctail

## License

### The MIT License (MIT)

Copyright © 2022 Mike Charlton

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

  - The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

  - THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
    OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.
